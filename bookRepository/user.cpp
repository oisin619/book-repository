#include "user.h"
#include <iostream>

User::User(string nameParam, string passParam)
{
	username = nameParam;
	password = passParam;
}
void User::setUsername(string nameParam)
{
	username = nameParam;
}
string User::getUsername() const
{
	return username;
}

void User::setPassword(string passParam)
{
	password = passParam;
}
string User::getPassword() const
{
	return password;
}
int User::loginData()
{
	//derived classes will put their own functionality into this method
	cout<<"This is a user"<<endl;
	return 0;
}