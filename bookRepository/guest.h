#ifndef GUEST_H
#define GUEST_H

#include <iostream>
#include "book.h"
#include<list>

//since guest doesn't have a username or password there is no need to inherit from User

class Guest
{
public:

	//Search function to search for a particular book
	void search(list<Book*> x,int choiceParam);

	
	//function to exectute when the user is logged in as a guest
	void login(list<Book*> x);

};

#endif