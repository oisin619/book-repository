#ifndef BOOK_H
#define BOOK_H

#include <string>
using namespace std;

class Book
{
private:
	string author;
	string title;
	int isbn;
	bool availability;

public:
	Book(string authParam,string titleParam,int isbnParam,bool availParam);

	void setAuthor(string authParam);

	string getAuthor() const;

	void setTitle(string titleParam);

	string getTitle() const;

	void setIsbn(int isbnParam);

	int getIsbn() const;

	void setAvail(bool availParam);

	bool getAvail() const;

};

#endif