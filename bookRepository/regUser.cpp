#include "regUser.h"
#include <iostream>


RegUser::RegUser(string userParam, string passParam) : User(userParam,passParam)
{
}


int RegUser::loginData()
{
	
	return 2;
}

void RegUser::search(list<Book*> x,int choiceParam)
{
	if(choiceParam == 1)
	{
		string title;
		cout<<"Enter the title of the book: "<<endl;
		cin>>title;
		//iterates through the book list until it finds the title that matches the index and prints out the books details
		for(list<Book*>::iterator it = x.begin(); it != x.end(); it++)
		{
			if(title == (*it)->getTitle())
			{
				cout<<(*it)->getTitle()<<endl;
				cout<<(*it)->getAuthor()<<endl;
				cout<<(*it)->getIsbn()<<endl;
			}
		}
	}
	else if(choiceParam == 2)
	{
		string author;
		cout<<"Enter the author of the book: "<<endl;
		cin>>author;
		//iterates through the book list until it finds the author that matches the index and prints out the books details
		for(list<Book*>::iterator it = x.begin(); it != x.end(); it++)
		{
			if(author == (*it)->getAuthor())
			{
				cout<<(*it)->getTitle()<<endl;
				cout<<(*it)->getAuthor()<<endl;
				cout<<(*it)->getIsbn()<<endl;
			}
		}
	}
	else if(choiceParam == 3)
	{
		int isbn;
		cout<<"Enter the author of the book: "<<endl;
		cin>>isbn;
		//iterates through the book list until it finds the isbn that matches the index and prints out the books details
		for(list<Book*>::iterator it = x.begin(); it != x.end(); it++)
		{
			if(isbn == (*it)->getIsbn())
			{
				cout<<(*it)->getTitle()<<endl;
				cout<<(*it)->getAuthor()<<endl;
				cout<<(*it)->getIsbn()<<endl;
			}
		}
	}
}

void RegUser::login(list<Book*> x)
{
	//asks for integer input to access different functions
	int choice = 0;
	cout<<"Which do you want to do? (1 for search, 2 for check availability)"<<endl;
	cin>>choice;
	//starts search feature. asks user which parameter they would like to search by
	if(choice = 1)
	{
		int choice2 = 0;
		cout<<"What parameter would you like to search?(1 for title,2 for author, 3 for ISBN)"<<endl;
		cin>>choice2;
		//calls the search function
		this->search(x,choice2);

	}
	//starts check availability feature to check if the book is available
	else if(choice == 2)
	{
		string book;
		cout<<"Enter  the name of the book: "<<endl;
		cin>>book;
		//iterates through the book linked list until the book string is the same as the index's getTitle() function
		for(list<Book*>::iterator it = x.begin(); it != x.end(); it++)
		{
			if(book == (*it)->getTitle())
			{
				bool avail = (*it)->getAvail();
				if(avail == false)
				{
					cout<<book<<" is not available"<<endl;
				}
				else if(avail == true)
				{
					cout<<book<<" is available"<<endl;
				}

			}
		}

	}
}


//returns the book to the repo abd removes it from the user's inventory
void RegUser::returnBook(list<Book*> x)
{
	
		string book;
		cout<<"Enter  the name of the book: "<<endl;
		cin>>book;
		for(list<Book*>::iterator it = x.begin(); it != x.end(); it++)
		{
			if(book == (*it)->getTitle())
			{
				(*it)->setAvail(true);
				checkedOut.remove((*it));

			}
		}
}

//checks out a book and adds it to the user's inventory
void RegUser::checkOut(list<Book*> x)
{
	string book;
		cout<<"Enter  the name of the book: "<<endl;
		cin>>book;
		for(list<Book*>::iterator it = x.begin(); it != x.end(); it++)
		{
			if(book == (*it)->getTitle())
			{
				(*it)->setAvail(false);
				checkedOut.push_back((*it));

			}
		}
}