#include "guest.h"
#include <iostream>
#include "book.h"
#include <list>

//function to search for books using either of its paramters to matchup.
void Guest::search(list<Book*> x,int choiceParam)
{
	if(choiceParam == 1)
	{
		string title;
		cout<<"Enter the title of the book: "<<endl;
		cin>>title;
		//iterates through the book list until it finds the title that matches the index and prints out the books details
		for(list<Book*>::iterator it = x.begin(); it != x.end(); it++)
		{
			if(title == (*it)->getTitle())
			{
				cout<<(*it)->getTitle()<<endl;
				cout<<(*it)->getAuthor()<<endl;
				cout<<(*it)->getIsbn()<<endl;
			}
		}
	}
	else if(choiceParam == 2)
	{
		string author;
		cout<<"Enter the author of the book: "<<endl;
		cin>>author;
		//iterates through the book list until it finds the author that matches the index and prints out the books details
		for(list<Book*>::iterator it = x.begin(); it != x.end(); it++)
		{
			if(author == (*it)->getAuthor())
			{
				cout<<(*it)->getTitle()<<endl;
				cout<<(*it)->getAuthor()<<endl;
				cout<<(*it)->getIsbn()<<endl;
			}
		}
	}
	else if(choiceParam == 3)
	{
		int isbn;
		cout<<"Enter the author of the book: "<<endl;
		cin>>isbn;
		//iterates through the book list until it finds the isbn that matches the index and prints out the books details
		for(list<Book*>::iterator it = x.begin(); it != x.end(); it++)
		{
			if(isbn == (*it)->getIsbn())
			{
				cout<<(*it)->getTitle()<<endl;
				cout<<(*it)->getAuthor()<<endl;
				cout<<(*it)->getIsbn()<<endl;
			}
		}
	}
}

//takes in the list of books as a parameter and asks which functionality would the user like to use
void Guest::login(list<Book*> x)
{
	//asks for integer input to access different functions
	int choice = 0;
	cout<<"Which do you want to do? (1 for search, 2 for check availability)"<<endl;
	cin>>choice;
	//starts search feature. asks user which parameter they would like to search by
	if(choice == 1)
	{
		int choice2 = 0;
		cout<<"What parameter would you like to search?(1 for title,2 for author, 3 for ISBN)"<<endl;
		cin>>choice2;
		//calls the search function
		this->search(x,choice2);

	}
	//starts check availability feature to check if the book is available
	else if(choice == 2)
	{
		string book;
		cout<<"Enter  the name of the book: "<<endl;
		cin>>book;
		//iterates through the book linked list until the book string is the same as the index's getTitle() function
		for(list<Book*>::iterator it = x.begin(); it != x.end(); it++)
		{
			if(book == (*it)->getTitle())
			{
				bool avail = (*it)->getAvail();
				if(avail == false)
				{
					cout<<book<<" is not available"<<endl;
				}
				else if(avail == true)
				{
					cout<<book<<" is available"<<endl;
				}

			}
		}

	}
}

