#ifndef REG_USER_H
#define REG_USER_H

#include "user.h"
#include "book.h"
#include <list>

class RegUser : public User
{
private:
	list<Book*> checkedOut;
public:
	RegUser(string userParam, string passParam);

	int loginData();

	void search(list<Book*> x,int choiceParam);

	void login(list<Book*> x);

	void checkOut(list<Book*> x);

	void returnBook(list<Book*> x);



};

#endif