#ifndef ADMIN_H
#define ADMIN_H

#include "user.h"
#include "book.h"
#include <list>

class Admin : public User
{
public:
	Admin(string userParam, string passParam);

	int loginData();

	void addBook(list<Book*> &repos, Book x);

	void removeBook(list<Book*> &repos, Book x);

	void markAvail(Book x,bool avail);

	void addUser(list<User*> &users, User x);

	void removeUser(list<User*> &users, User x);

};

#endif