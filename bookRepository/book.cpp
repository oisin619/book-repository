#include "Book.h"
#include <string>

using namespace std;

//constructor for book
Book::Book(string authParam,string titleParam,int isbnParam,bool availParam)
{
	author = authParam;
	title = titleParam;
	isbn = isbnParam;
	availability = availParam;
}


//mutators and accessors for book variables
void Book::setAuthor(string authParam)
{
	author = authParam;
}

string Book::getAuthor() const
{
	return author;
}

void Book::setTitle(string titleParam)
{
	title = titleParam;
}

string Book::getTitle() const
{
	return title;
}

void Book::setIsbn(int isbnParam)
{
	isbn = isbnParam;
}

int Book::getIsbn() const
{
	return isbn;
}


void Book::setAvail(bool availParam)
{
	availability = availParam;
}
bool Book::getAvail() const
{
	return availability;
}