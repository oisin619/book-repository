#ifndef USER_H
#define USER_H

#include <string>

using namespace std;

class User
{
	//declare variables for all users
private:
	string username;
	string password;
	bool login;

public:
	//constructor
	User(string nameParam, string passParam);

	//mutator and accessor for username
	void setUsername(string userParam);
	string getUsername() const;

	//mutator and accessor for password
	void setPassword(string passParam);
	string getPassword() const;

	//function to detect which type of user is logging in
	virtual int loginData();
};

#endif