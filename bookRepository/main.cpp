#include <iostream>
#include<list>
#include "user.h"
#include "admin.h"
#include "guest.h"
#include "book.h"
#include "regUser.h"
#include <vector>

using namespace std;

//function that logs in the user
int userLogin(list<User*> x,string userParam,string passParam)
{
	
	int loopCheck = 0;
	list<User*>::iterator pos2;
	//iterates through the list of users until it finds an idex where the password and username are the same as what the user inputted
	for(pos2 = x.begin();pos2 != x.end();pos2++)
	{
		if((*pos2)->getUsername() == userParam && (*pos2)->getPassword() == passParam)
		{
			cout<<(*pos2)->getUsername()<<" is logged in"<<endl;
			//calls the loginData function to determine which type of user is logged in
			loopCheck = 1;
			(*pos2)->loginData();
	
		}
	}
	if(loopCheck != 1)
	{
		//if the user enters in the incorrect name or password, the login fails
		cout<<"Login failed: try again"<<endl;
		return -1;
	}
		

}


//function to start the intial login and ask for user details
int initLogin(list<User*> x)
{
	string user;
	string password;
	bool loggedIn = false;

	while(loggedIn != true)
	{
				cout<<"Enter your username: "<<endl;
				cin>>user;
				cout<<"Enter your password: "<<endl;
				cin>>password;
				loggedIn =userLogin(x,user,password);
	}
	//returns the loginData result which determines which type of user is logging in
	return loggedIn;
}



void main()
{
	//list of all users
	RegUser one = RegUser("Simon","ehehe");
	RegUser two = RegUser("Jake","ohohoh");
	RegUser three = RegUser("Neill", "nooooo");
	Admin me = Admin("Oisin","nightmare");
	Admin thatGuy = Admin("Nick","hearth");
	Guest guest = Guest();

	//list of books. Note that not all of these are located in the repository
	Book lotr = Book("JRRT","TheLordOfTheRings",1,true);
	Book hp = Book("JKR","HarryPotter",2,true);
	Book hg = Book("SC","TheHungerGames",3,true);
	Book got = Book("GRRM","AGameOfThrones",4,false);
	Book bh = Book("TK","Bleach",5,false);

	//a vector to help iterate through the total books when trying to add or remove them from the repository. This is not the repository itself
	vector<Book*> bookList;
	bookList.push_back(&lotr);
	bookList.push_back(&hp);
	bookList.push_back(&hg);
	bookList.push_back(&got);
	bookList.push_back(&bh);

	vector<User*> userList;
	userList.push_back(&one);
	userList.push_back(&two);
	userList.push_back(&three);
	userList.push_back(&me);
	userList.push_back(&thatGuy);



	

	//variables for the user logging in.
	string user;
	string password;
	string userType;
	int loggedIn =0;
	bool finished = false;


	//linked list for the user and books along with the methods to add the objects to their respective lists
	list<User*> users;
	list<Book*> books;
	users.push_back(&one);
	users.push_back(&two);
	users.push_back(&me);

	books.push_back(&lotr);
	books.push_back(&hp);
	books.push_back(&hg);

	cout<<"Welcome to the Great Library!"<<endl;

	//asks the user if they are a guest or not
	cout<<"Are you a registered user?(Y/N)"<<endl;
	cin>>userType;
	

	//loop to keep the program running so mulitple tasks can be executed
	while(finished != true)
	{
		//if user is registerd, asks to input info to determine which type of user they are
		if(userType =="y")
		{
				loggedIn =initLogin(users);
				if(loggedIn == 1)
				{
					//sets current administrator as the person logged in
					Admin* currAdmin = &me;
					list<User*>::iterator pos2;
					for(pos2 = users.begin();pos2 != users.end();pos2++)
					{	
						if((*pos2)->getUsername()==user)
						{
							currAdmin == (*pos2);
						}
					}
					
					// asks the admin which feature they would like to access
					int choice;
					cout<<"Welcome! Enter 1 to add book, 2 to remove book,3 to mark availability, 4 to add user, 5 to remove user: "<<endl;
					cin>>choice;

					if(choice ==1)
					{
						//adds a book by iterating through the vector of books until the string book equals the index. that book is then added to the books list
						string book;
						cout<<"Enter  the name of the book: "<<endl;
						cin>>book;
						for(vector<Book*>::iterator it = bookList.begin(); it != bookList.end(); it++)
						{
							if(book == (*it)->getTitle())
							{
								books.push_back((*it));
								cout<<book<<" added to repository"<<endl;
							}
						}
								
						
					}
					else if(choice == 2)
					{
						//removes a book by iterating through the vector of books until the string book equals the index. that book is then removed from the books list
						string book;
						cout<<"Enter  the name of the book: "<<endl;
						cin>>book;
						for(list<Book*>::iterator it = books.begin(); it != books.end(); it++)
						{
							if(book == (*it)->getTitle())
							{
								books.remove((*it));
								cout<<book<<" removed from repository"<<endl;
							}
						}
					}
					else if(choice == 3)
					{
						//checks a book's availability by iterating through the vector of books until the string book equals the index. the checkAvail method is then called for that book
						string book;
						cout<<"Enter  the name of the book: "<<endl;
						cin>>book;
						for(list<Book*>::iterator it = books.begin(); it != books.end(); it++)
						{
							if(book == (*it)->getTitle())
							{
								string avail;
								cout<<"Is this book available?: "<<endl;
								cin>>avail;
								if(avail == "y")
								{
									(*it)->setAvail(true);
									cout<<book<<" is available"<<endl;
								}
							}
						}
					}
					else if(choice == 4)
					{
						//adds a user to the users list by iterating through the users vector until the name matches the index, then the user is added to the repo
						string user;
						cout<<"Enter  the name of the User: "<<endl;
						cin>>user;
						for(vector<User*>::iterator it = userList.begin(); it != userList.end(); it++)
						{
							if(user == (*it)->getUsername())
							{
								users.push_back((*it));
								cout<<user<<" added to user directory"<<endl;
							}
						}
					}
					else if(choice == 5)
					{
						//removes a user from the repo by iterating though the users list until the name matches the index, the user is then removed from the linked list
						string user;
						cout<<"Enter  the name of the User: "<<endl;
						cin>>user;
						for(list<User*>::iterator it = users.begin(); it != users.end(); it++)
						{
							if(user == (*it)->getUsername())
							{
								users.remove((*it));
								cout<<user<<" removed from user directory"<<endl;
							}
						}
					}


				
				
				}
				//if the loggin is of type RegUser, then the current user is set to the user and the login function is called
				else if(loggedIn == 2)
				{
					RegUser* currUser = &one;
					list<User*>::iterator pos2;
					for(pos2 = users.begin();pos2 != users.end();pos2++)
					{	
						if((*pos2)->getUsername()==user)
						{
							currUser == (*pos2);
						}
					}
					//calls all the RegUser's functionality
					currUser->login(books);
				}
		}
		//if the user is not registered then they are guests
		else if(userType == "n")
		{
			//calls the guest's functions
			guest.login(books);
		}
		//the user enters the wrong information
		else
		{
			cout<<"You did not enter a valid response"<<endl;
		}
	}






	system("pause");
}